package com.example.aplikasi1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_verif.bt_bck_1

class VerifActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verif)
        supportActionBar?.hide()

        btnLoginListener ()
    }

    private fun btnLoginListener() {
        bt_bck_1.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

    }
}