package com.example.aplikasi1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.l_btn_1
import kotlinx.android.synthetic.main.activity_login.l_img_1

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        btnBackLoginListener()
    }
    private fun btnBackLoginListener() {
        l_img_1.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))


        }

        btnVerifListener()
    }
    private fun btnVerifListener(){
        l_btn_1.setOnClickListener {
            startActivity(Intent(this, VerifActivity::class.java))
        }
    }
}

